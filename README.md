# README #

This is a simple script to enable automatic refresh on git [tig](https://github.com/jonas/tig). There is much room for improvements. Feel free to provide your feedback :)

---

## Dependencies:

1. [xdotools](http://www.semicomplete.com/projects/xdotool)

2. [inotify](http://man7.org/linux/man-pages/man7/inotify.7.html)

## Instructions:

1. ```sudo ln -s /full/path/to/script /usr/local/bin/executable_name``` (optional)

2. ```chmod +x /full/path/to/script```

3. ```executable_name path-of-repo```

## Known Issues:

1. Spawns new terminal for tig. Should be able to run it in the same from where the script is called.

2. Tense flickering when tig window is not on top.