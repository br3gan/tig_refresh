#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Declare repo path"
    exit
fi

path="$1/.gitignore"
term_pid=$$
gnome-terminal -x sh -c "cd $1 && tig && kill $term_pid;";

tig_window=$(xdotool getwindowfocus)
# tig_window=`xdotool search "tig" | head -n1`

tig_pid=`pidof tig`
while true
do  sleep 3
    inotifywait "$1/.gitignore"
    #inotifywait -e MODIFY,DELETE,CREATE $1
    focused_window_id=$(xdotool getwindowfocus)
    pid=`pidof tig`
    if [ "$pid" != "$tig_pid" ]; then
        exit
    fi
    xdotool windowactivate -sync $tig_window
    xdotool key F5;
    xdotool windowactivate -sync $focused_window_id
done;
